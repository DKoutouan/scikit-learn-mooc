{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e2f157c1",
   "metadata": {},
   "source": [
    "# The blood transfusion dataset\n",
    "\n",
    "In this notebook, we will present the \"blood transfusion\" dataset. This\n",
    "dataset is locally available in the directory `datasets` and it is stored as\n",
    "a comma separated value (CSV) file. We start by loading the entire dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce7f5fd7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "blood_transfusion = pd.read_csv(\"../datasets/blood_transfusion.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "622a00dd",
   "metadata": {},
   "source": [
    "We can have a first look at the at the dataset loaded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd476f36",
   "metadata": {},
   "outputs": [],
   "source": [
    "blood_transfusion.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "504d7a17",
   "metadata": {},
   "source": [
    "In this dataframe, we can see that the last column correspond to the target\n",
    "to be predicted called `\"Class\"`. We will create two variables, `data` and\n",
    "`target` to separate the data from which we could learn a predictive model\n",
    "and the `target` that should be predicted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0e287c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = blood_transfusion.drop(columns=\"Class\")\n",
    "target = blood_transfusion[\"Class\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2809673",
   "metadata": {},
   "source": [
    "Let's have a first look at the `data` variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0bfc5fe2",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80052f79",
   "metadata": {},
   "source": [
    "We observe four columns. Each record corresponds to a person that intended\n",
    "to give blood. The information stored in each column are:\n",
    "\n",
    "* `Recency`: the time in months since the last time a person intended to\n",
    "  give blood;\n",
    "* `Frequency`: the number of time a person intended to give blood in the\n",
    "   past;\n",
    "* `Monetary`: the amount of blood given in the past (in c.c.);\n",
    "* `Time`: the time in months since the first time a person intended to give\n",
    "  blood.\n",
    "\n",
    "Now, let's have a look regarding the type of data that we are dealing in\n",
    "these columns and if any missing values are present in our dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d823b908",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e5bd69d",
   "metadata": {},
   "source": [
    "Our dataset is made of 748 samples. All features are represented with integer\n",
    "numbers and there is no missing values. We can have a look at each feature\n",
    "distributions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74f79a99",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = data.hist(figsize=(12, 10), bins=30, edgecolor=\"black\", density=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13a3df12",
   "metadata": {},
   "source": [
    "There is nothing shocking regarding the distributions. We only observe a high\n",
    "value range for the features `\"Recency\"`, `\"Frequency\"`, and `\"Monetary\"`. It\n",
    "means that we have a few extreme high values for these features.\n",
    "\n",
    "Now, let's have a look at the target that we would like to predict for this\n",
    "task."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3f90ccd",
   "metadata": {},
   "outputs": [],
   "source": [
    "target.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73afd4c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "target.value_counts(normalize=True).plot.barh()\n",
    "plt.xlabel(\"Number of samples\")\n",
    "_ = plt.title(\"Class distribution\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c39912bb",
   "metadata": {},
   "source": [
    "We see that the target is discrete and contains two categories: whether a\n",
    "person `\"donated\"` or `\"not donated\"` his/her blood. Thus the task to be\n",
    "solved is a classification problem. We should note that the class counts of\n",
    "these two classes is different."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "92a0251a",
   "metadata": {},
   "outputs": [],
   "source": [
    "target.value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81d355bf",
   "metadata": {},
   "source": [
    "Indeed, ~76% of the samples belong to the class `\"not donated\"`. It is rather\n",
    "important: a classifier that would predict always this `\"not donated\"` class\n",
    "would achieve an accuracy of 76% of good classification without using any\n",
    "information from the data itself. This issue is known as class imbalance. One\n",
    "should take care about the statistical performance metric used to evaluate a\n",
    "model as well as the predictive model chosen itself.\n",
    "\n",
    "Now, let's have a naive analysis to see if there is a link between features\n",
    "and the target using a pair plot representation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c1040ee2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "\n",
    "_ = sns.pairplot(blood_transfusion, hue=\"Class\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6feea9a2",
   "metadata": {},
   "source": [
    "Looking at the diagonal plots, we don't see any feature that individually\n",
    "could help at separating the two classes. When looking at a pair of feature,\n",
    "we don't see any stricking combinations as well. However, we can note that\n",
    "the `\"Monetary\"` and `\"Frequency\"` features are perfectly correlated: all the\n",
    "data points are aligned on a diagonal.\n",
    "\n",
    "As a conclusion, this dataset would be a challenging dataset: it suffer from\n",
    "class imbalance, correlated features and thus very few features will be\n",
    "available to learn a model, and none of the feature combinations were found\n",
    "to help at predicting."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  },
  "nbreset": "https://github.com/INRIA/scikit-learn-mooc/raw/master/notebooks/datasets_blood_transfusion.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
